
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
import {gCoursesDB} from './script.js'
// mảng lưu thông tin các khóa học most Popular
var gPopularCoursesDB = [];
// mảng lưu thông tin các khóa học trending
var gTrendingCoursesDB = [];
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    //thêm event handler cho nút Browse course
    $("#btn-browse-course").click(function () {
        onBtnBrowseCourseClick();
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm xử lý khi tải trang
function onPageLoading() {
    "use strict";
    console.log("Load trang tại đây");
    // Lấy danh sách khóa học popular Couses
    gPopularCoursesDB = getDataPopularCourse(gCoursesDB.courses);
    console.log("Thông tin các khóa học popurlar");
    console.log(gPopularCoursesDB);
    //Load thông tin khóa học popular ra html
    var vDivPopularCourse = $("#div-popular-course");
    showDataCourseToHtml(vDivPopularCourse, gPopularCoursesDB);

    // Lấy danh sách khóa học Trending Couses
    gTrendingCoursesDB = getDataTrendingCourse(gCoursesDB.courses);
    console.log("Thông tin các khóa học trending");
    console.log(gTrendingCoursesDB);
    // Load thông tin khóa khọc trending ra html
    var vDivTrendingCourse = $("#div-trending-course");
    showDataCourseToHtml(vDivTrendingCourse, gTrendingCoursesDB);
}
// Hàm xử lý khi nhấn nút Browse course
function onBtnBrowseCourseClick() {
    "use strict";
    console.log("Nút browse course được ấn");
    var vDETAIL_URL_FORM = "browseCourse.html";
    window.location.href = vDETAIL_URL_FORM;
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm xử lý lấy các khóa học Most popular
function getDataPopularCourse(paramCousesDB) {
    "use strict";
    var vPopularObj = paramCousesDB.filter(course => course.isPopular === true);
    return vPopularObj;
}
// Hàm xử lý lấy các khóa học Trending
function getDataTrendingCourse(paramCousesDB) {
    "use strict";
    var vTrendingObj = paramCousesDB.filter(course => course.isTrending === true);
    return vTrendingObj;
}
//Hàm xử lý hiển thị thông tin khóa học ra html
function showDataCourseToHtml(paramElement, paramCousesDB) {
    "use strict";
    $(paramElement).html(""); // xóa trắng thông tin html
    for (var bI = 0; bI < paramCousesDB.length; bI++) {
        var vContent =
            `<div class="col-sm-3">
              <div class="card">
                  <img src= ${paramCousesDB[bI].coverImage} class="card-img-top">
                  <div class="card-body">
                      <p class="card-text text-primary">${paramCousesDB[bI].courseName}</p>
                      <p><i class="far fa-clock"></i> &nbsp; ${paramCousesDB[bI].duration + " " + paramCousesDB[bI].level}</p>
                      <p><b>${paramCousesDB[bI].discountPrice}</b> <del>${paramCousesDB[bI].price}</del></p> 
                  </div>
                  <div class="card-footer text-muted">
                      <div class="row">
                          <div class="col-sm-10">
                              <img src=${paramCousesDB[bI].teacherPhoto} width="35px" height="35px" class="img-teacher"> ${paramCousesDB[bI].teacherName}
                          </div> 
                          <div class="col-sm-2">
                              <i class="far fa-bookmark icon-bookmark"></i>
                          </div>
                      </div>
                  </div>
              </div>
          </div>`;
        $(vContent).appendTo(paramElement);
    }
}